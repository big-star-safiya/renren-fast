package io.renren.modules.message.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.message.entity.MessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.message.entity.MessageEntityVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 16:10:44
 */
@Mapper
public interface MessageDao extends BaseMapper<MessageEntity> {

    IPage<MessageEntityVO> getPage(Page<MessageEntityVO> page, @Param("mesContent") String mesContent);
}
