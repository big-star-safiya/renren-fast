package io.renren.modules.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.message.entity.MessageEntity;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 16:10:44
 */
public interface MessageService extends IService<MessageEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);
}

