package io.renren.modules.message.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.message.entity.MessageEntityVO;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.message.dao.MessageDao;
import io.renren.modules.message.entity.MessageEntity;
import io.renren.modules.message.service.MessageService;


@Service("messageService")
public class MessageServiceImpl extends ServiceImpl<MessageDao, MessageEntity> implements MessageService {

    @Autowired MessageDao messageDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MessageEntity> page = this.page(
                new Query<MessageEntity>().getPage(params),
                new QueryWrapper<MessageEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String mesContent = params.get("mesContent").toString();
        Page<MessageEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<MessageEntityVO> getUser = messageDao.getPage(page, mesContent);
        return new PageUtils(getUser);
    }

}