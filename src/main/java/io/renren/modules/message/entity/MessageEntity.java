package io.renren.modules.message.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 16:10:44
 */
@Data
@TableName("mind_message")
public class MessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 留言id
	 */
	@TableId
	private Long mesId;
	/**
	 * 内容
	 */
	private String mesContent;
	/**
	 * 外键留言人
	 */
	private Long anonId;
	/**
	 * 是否删除
	 */
	@TableLogic(value = "0", delval = "-1")
	@TableField(value = "deleted", fill = FieldFill.INSERT)
	private Integer deleted;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
