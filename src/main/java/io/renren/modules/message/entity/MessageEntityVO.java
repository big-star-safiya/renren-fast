package io.renren.modules.message.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * ClassName: MessageEntityVO
 * Description:
 *
 * @Author LiuXingyu
 * @Create 2023/4/17 16:19
 * @Version 1.0
 */
@Data
@TableName("mind_message")
public class MessageEntityVO extends MessageEntity {
    private String anonname;
}
