package io.renren.modules.option.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.option.entity.OptionEntity;

import java.util.List;
import java.util.Map;

/**
 * 选项管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 17:25:20
 */
public interface OptionService extends IService<OptionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<OptionEntity> getAllByQueId(Long queId);
}

