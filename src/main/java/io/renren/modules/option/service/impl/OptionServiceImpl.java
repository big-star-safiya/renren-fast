package io.renren.modules.option.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.option.dao.OptionDao;
import io.renren.modules.option.entity.OptionEntity;
import io.renren.modules.option.service.OptionService;


@Service("optionService")
public class OptionServiceImpl extends ServiceImpl<OptionDao, OptionEntity> implements OptionService {

    @Autowired
    OptionDao optionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OptionEntity> page = this.page(
                new Query<OptionEntity>().getPage(params),
                new QueryWrapper<OptionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<OptionEntity> getAllByQueId(Long queId) {
        return optionDao.getAllByQueId(queId);
    }


}