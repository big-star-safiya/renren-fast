package io.renren.modules.option.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.option.entity.OptionEntity;
import io.renren.modules.option.service.OptionService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 选项管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 17:25:20
 */
@RestController
@RequestMapping("que/option")
public class OptionController {
    @Autowired
    private OptionService optionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("que:option:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = optionService.queryPage(params);
//        List<OptionEntity> options = optionService.getAllByQueId(queId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{optionId}")
    @RequiresPermissions("que:option:info")
    public R info(@PathVariable("optionId") Long optionId){
		OptionEntity option = optionService.getById(optionId);

        return R.ok().put("option", option);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("que:option:save")
    public R save(@RequestBody OptionEntity option){
		optionService.save(option);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("que:option:update")
    public R update(@RequestBody OptionEntity option){
		optionService.updateById(option);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("que:option:delete")
    public R delete(@RequestBody Long[] optionIds){
		optionService.removeByIds(Arrays.asList(optionIds));

        return R.ok();
    }

}
