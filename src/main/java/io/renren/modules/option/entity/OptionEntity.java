package io.renren.modules.option.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 选项管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 17:25:20
 */
@Data
@TableName("mind_option")
public class OptionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 选项id
	 */
	@TableId
	private Long optionId;
	/**
	 * 选项内容
	 */
	private String optionContent;
	/**
	 * 选项分值
	 */
	private Integer optionScore;
	/**
	 * 外键问题id
	 */
	private Long queId;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
