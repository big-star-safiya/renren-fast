package io.renren.modules.option.dao;

import io.renren.modules.option.entity.OptionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 选项管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 17:25:20
 */
@Mapper
public interface OptionDao extends BaseMapper<OptionEntity> {

    List<OptionEntity> getAllByQueId(@Param("queId") Long optionId);
}
