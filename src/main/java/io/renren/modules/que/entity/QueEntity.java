package io.renren.modules.que.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 题目管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 17:25:13
 */
@Data
@TableName("mind_que")
public class QueEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 问题id
	 */
	@TableId
	private Long queId;
	/**
	 * 题目内容
	 */
	private String queContent;
	/**
	 * 外键题目类型
	 */
	private Long qtypeId;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
