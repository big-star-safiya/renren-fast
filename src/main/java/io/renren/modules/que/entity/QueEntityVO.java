package io.renren.modules.que.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * ClassName: QueEntityVO
 * Description:
 *
 * @Author LiuXingyu
 * @Create 2023/4/17 17:32
 * @Version 1.0
 */
@Data
@TableName("mind_que")
public class QueEntityVO extends QueEntity{
    private String qtypename;
}
