package io.renren.modules.que.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.option.entity.OptionEntity;
import lombok.Data;

import java.util.List;

/**
 * ClassName: QueOptionEntity
 * Description:
 *
 * @Author LiuXingyu
 * @Create 2023/4/24 15:48
 * @Version 1.0
 */
@Data
@TableName("mind_que")
public class QueOptionEntity extends QueEntity {
    private List<OptionEntity> options;
}
