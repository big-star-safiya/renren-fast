package io.renren.modules.que.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.renren.modules.option.entity.OptionEntity;
import io.renren.modules.option.service.OptionService;
import io.renren.modules.que.entity.QueOptionEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.que.entity.QueEntity;
import io.renren.modules.que.service.QueService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 题目管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 17:25:13
 */
@RestController
@RequestMapping("que/que")
public class QueController {
    @Autowired
    private QueService queService;
    @Autowired
    private OptionService optionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("que:que:list")
    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = queService.queryPage(params);
        PageUtils page = queService.getPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{queId}")
    @RequiresPermissions("que:que:info")
    public R info(@PathVariable("queId") Long queId){
		QueEntity que = queService.getById(queId);
        List<OptionEntity> options = optionService.getAllByQueId(queId);
        return R.ok().put("que", que).put("options", options);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("que:que:save")
    public R save(@RequestBody QueOptionEntity queOption){
//		queService.save(que);
        queService.saveQueOption(queOption);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("que:que:update")
    public R update(@RequestBody QueOptionEntity queOption){
//		queService.updateById(que);
        queService.updateQueOption(queOption);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("que:que:delete")
    public R delete(@RequestBody Long[] queIds){
		queService.removeByIds(Arrays.asList(queIds));

        return R.ok();
    }

}
