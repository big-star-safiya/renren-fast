package io.renren.modules.que.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.que.entity.QueEntity;
import io.renren.modules.que.entity.QueOptionEntity;

import java.util.Map;

/**
 * 题目管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 17:25:13
 */
public interface QueService extends IService<QueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);

    void updateQueOption(QueOptionEntity queOption);

    void saveQueOption(QueOptionEntity queOption);
}

