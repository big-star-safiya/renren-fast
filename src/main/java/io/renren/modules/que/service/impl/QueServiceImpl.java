package io.renren.modules.que.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.option.entity.OptionEntity;
import io.renren.modules.option.service.OptionService;
import io.renren.modules.que.entity.QueEntityVO;
import io.renren.modules.que.entity.QueOptionEntity;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.que.dao.QueDao;
import io.renren.modules.que.entity.QueEntity;
import io.renren.modules.que.service.QueService;


@Service("queService")
public class QueServiceImpl extends ServiceImpl<QueDao, QueEntity> implements QueService {

    @Autowired QueDao queDao;
    @Autowired OptionService optionService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QueEntity> page = this.page(
                new Query<QueEntity>().getPage(params),
                new QueryWrapper<QueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String queContent = params.get("queContent").toString();
        String qtypename = params.get("qtypename").toString();
        Page<QueEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<QueEntityVO> getUser = queDao.getPage(page, queContent, qtypename);
        return new PageUtils(getUser);
    }

    @Override
    public void updateQueOption(QueOptionEntity queOption) {
        List<OptionEntity> options = queOption.getOptions();
        QueEntity que = queOption;
        options.forEach(opt -> {
            optionService.updateById(opt);
        });
        updateById(que);
    }

    @Override
    public void saveQueOption(QueOptionEntity queOption) {
        QueEntity que = queOption;
        save(que);
        Long queId = que.getQueId();
        List<OptionEntity> options = queOption.getOptions();
        options.forEach(opt -> {
            opt.setQueId(queId);
            optionService.save(opt);
        });
    }

}