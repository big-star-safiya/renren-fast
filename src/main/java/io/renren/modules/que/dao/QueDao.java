package io.renren.modules.que.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.que.entity.QueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.que.entity.QueEntityVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 题目管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 17:25:13
 */
@Mapper
public interface QueDao extends BaseMapper<QueEntity> {

    IPage<QueEntityVO> getPage(Page<QueEntityVO> page, @Param("queContent")String queContent, @Param("qtypename")String qtypename);
}
