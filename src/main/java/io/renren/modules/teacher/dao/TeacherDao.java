package io.renren.modules.teacher.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.teacher.entity.TeacherEntity;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-16 15:03:25
 */
@Mapper
public interface TeacherDao extends BaseMapper<TeacherEntity> {

    IPage<TeacherEntityVO> getPage(IPage<TeacherEntityVO> page, @Param("teacherName") String teacherName, @Param("schoolName") String schoolName);

}
