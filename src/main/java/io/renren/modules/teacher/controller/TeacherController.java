package io.renren.modules.teacher.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.modules.teacher.entity.TeacherEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.teacher.entity.TeacherEntityVO;
import io.renren.modules.teacher.service.TeacherService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-16 15:03:25
 */
@RestController
@RequestMapping("teacher/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("teacher:teacher:list")
    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = teacherService.queryPage(params);
//        PageUtils page = teacherService.queryAll();
        PageUtils page = teacherService.getPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{teacherId}")
    @RequiresPermissions("teacher:teacher:info")
    public R info(@PathVariable("teacherId") Long teacherId){
		TeacherEntity teacher = teacherService.getById(teacherId);

        return R.ok().put("teacher", teacher);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("teacher:teacher:save")
    public R save(@RequestBody TeacherEntity teacher){
		teacherService.save(teacher);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("teacher:teacher:update")
    public R update(@RequestBody TeacherEntity teacher){
		teacherService.updateById(teacher);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("teacher:teacher:delete")
    public R delete(@RequestBody Long[] teacherIds){
		teacherService.removeByIds(Arrays.asList(teacherIds));

        return R.ok();
    }

}
