package io.renren.modules.teacher.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-16 15:03:25
 */
@Data
@TableName("mind_teacher")
public class TeacherEntityVO extends TeacherEntity implements Serializable {
	private String schoolname;
}
