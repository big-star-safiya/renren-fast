package io.renren.modules.teacher.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 *
 * @author
 * @email
 * @date 2023-04-16 15:03:25
 */
@Data
@TableName("mind_teacher")
public class TeacherEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 老师id
     */
    @TableId
    private Long teacherId;
    /**
     * 老师姓名
     */
    private String teachername;
    /**
     * 密码
     */
    private String password;
    /**
     * 外键
     */
    private Long schoolId;

}
