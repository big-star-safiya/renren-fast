package io.renren.modules.teacher.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.teacher.entity.TeacherEntity;
import io.renren.modules.teacher.entity.TeacherEntityVO;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-16 15:03:25
 */
public interface TeacherService extends IService<TeacherEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);
}

