package io.renren.modules.teacher.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.teacher.entity.TeacherEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.teacher.dao.TeacherDao;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import io.renren.modules.teacher.service.TeacherService;


@Service("teacherService")
public class TeacherServiceImpl extends ServiceImpl<TeacherDao, TeacherEntity> implements TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        IPage<TeacherEntity> page = this.page(
                new Query<TeacherEntity>().getPage(params),
                new QueryWrapper<TeacherEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String teachername = params.get("teachername").toString();
        String schoolname = params.get("schoolname").toString();
        Page<TeacherEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<TeacherEntityVO> getUser = teacherDao.getPage(page, teachername, schoolname);
        return new PageUtils(getUser);
    }

}