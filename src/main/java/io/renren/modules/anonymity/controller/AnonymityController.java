package io.renren.modules.anonymity.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.modules.anonymity.entity.AnonymityEntityVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.anonymity.entity.AnonymityEntity;
import io.renren.modules.anonymity.service.AnonymityService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 15:05:43
 */
@RestController
@RequestMapping("anonymity/anonymity")
public class AnonymityController {
    @Autowired
    private AnonymityService anonymityService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("anonymity:anonymity:list")
    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = anonymityService.queryPage(params);
        PageUtils page = anonymityService.getPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{anonId}")
    @RequiresPermissions("anonymity:anonymity:info")
    public R info(@PathVariable("anonId") Long anonId){
//        AnonymityEntity anonymity = anonymityService.getById(anonId);
        AnonymityEntityVO anonymity = anonymityService.getInfoById(anonId);

        return R.ok().put("anonymity", anonymity);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("anonymity:anonymity:save")
    public R save(@RequestBody AnonymityEntity anonymity){
		anonymityService.save(anonymity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("anonymity:anonymity:update")
    public R update(@RequestBody AnonymityEntity anonymity){
		anonymityService.updateById(anonymity);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("anonymity:anonymity:delete")
    public R delete(@RequestBody Long[] anonIds){
		anonymityService.removeByIds(Arrays.asList(anonIds));

        return R.ok();
    }

}
