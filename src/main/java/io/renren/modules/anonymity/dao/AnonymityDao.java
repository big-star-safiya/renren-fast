package io.renren.modules.anonymity.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.anonymity.entity.AnonymityEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.anonymity.entity.AnonymityEntityVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 15:05:43
 */
@Mapper
public interface AnonymityDao extends BaseMapper<AnonymityEntity> {

    IPage<AnonymityEntityVO> getPage(Page<AnonymityEntityVO> page, @Param("anonname") String anonname);

    AnonymityEntityVO getInfoById(Long anonId);
}
