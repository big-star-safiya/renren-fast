package io.renren.modules.anonymity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.anonymity.entity.AnonymityEntity;
import io.renren.modules.anonymity.entity.AnonymityEntityVO;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 15:05:43
 */
public interface AnonymityService extends IService<AnonymityEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);

    AnonymityEntityVO getInfoById(Long anonId);
}

