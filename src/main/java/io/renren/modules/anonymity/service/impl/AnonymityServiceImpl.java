package io.renren.modules.anonymity.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.anonymity.entity.AnonymityEntityVO;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.anonymity.dao.AnonymityDao;
import io.renren.modules.anonymity.entity.AnonymityEntity;
import io.renren.modules.anonymity.service.AnonymityService;


@Service("anonymityService")
public class AnonymityServiceImpl extends ServiceImpl<AnonymityDao, AnonymityEntity> implements AnonymityService {

    @Autowired AnonymityDao anonymityDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AnonymityEntity> page = this.page(
                new Query<AnonymityEntity>().getPage(params),
                new QueryWrapper<AnonymityEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String anonname = params.get("anonname").toString();
        Page<AnonymityEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<AnonymityEntityVO> getUser = anonymityDao.getPage(page, anonname);
        return new PageUtils(getUser);
    }

    @Override
    public AnonymityEntityVO getInfoById(Long anonId){
        return anonymityDao.getInfoById(anonId);
    }

}