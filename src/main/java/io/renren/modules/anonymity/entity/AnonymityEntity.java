package io.renren.modules.anonymity.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 15:05:43
 */
@Data
@TableName("mind_anonymity")
public class AnonymityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 匿名id
	 */
	@TableId
	private Long anonId;
	/**
	 * 匿名
	 */
	private String anonname;
	/**
	 * 外键学生id
	 */
	private Long studentId;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
