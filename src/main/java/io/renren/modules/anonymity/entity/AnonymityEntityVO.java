package io.renren.modules.anonymity.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * ClassName: AnonymityEntityVO
 * Description:
 *
 * @Author LiuXingyu
 * @Create 2023/4/17 15:09
 * @Version 1.0
 */
@Data
@TableName("mind_anonymity")
public class AnonymityEntityVO extends AnonymityEntity{
    private String studentname;
}
