package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.AdminEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 00:23:31
 */
@Mapper
public interface AdminDao extends BaseMapper<AdminEntity> {
    AdminEntity queryByName(@Param("adminname")String name);
}
