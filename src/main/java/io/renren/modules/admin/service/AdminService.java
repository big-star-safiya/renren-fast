package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.AdminEntity;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 00:23:31
 */
public interface AdminService extends IService<AdminEntity> {

    PageUtils queryPage(Map<String, Object> params);

    AdminEntity queryByName(String name);
}

