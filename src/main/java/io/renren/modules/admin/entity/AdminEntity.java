package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 00:23:31
 */
@Data
@TableName("mind_admin")
public class AdminEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 管理员id
	 */
	@TableId
	private Long adminId;
	/**
	 * 管理员名
	 */
	private String adminname;
	/**
	 * 密码
	 */
	private String password;

}
