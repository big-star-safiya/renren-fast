package io.renren.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.teacher.entity.TeacherEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.knowledge.dao.KnowDao;
import io.renren.modules.knowledge.entity.KnowEntity;
import io.renren.modules.knowledge.service.KnowService;


@Service("knowService")
public class KnowServiceImpl extends ServiceImpl<KnowDao, KnowEntity> implements KnowService {

    @Autowired KnowDao knowDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KnowEntity> page = this.page(
                new Query<KnowEntity>().getPage(params),
                new QueryWrapper<KnowEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String knowTitle = params.get("knowTitle").toString();
        String knowContent = params.get("knowContent").toString();
        Page<KnowEntity> page = new Page<>(pageNum, pageSize);
        IPage<KnowEntity> getUser = knowDao.getPage(page, knowTitle, knowContent);
        return new PageUtils(getUser);
    }

}