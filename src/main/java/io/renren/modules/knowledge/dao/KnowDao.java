package io.renren.modules.knowledge.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.knowledge.entity.KnowEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 11:26:11
 */
@Mapper
public interface KnowDao extends BaseMapper<KnowEntity> {

    IPage<KnowEntity> getPage(Page<KnowEntity> page, @Param("knowTitle") String knowTitle, @Param("knowContent") String knowContent);
}
