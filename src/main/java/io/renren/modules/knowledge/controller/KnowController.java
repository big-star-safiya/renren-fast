package io.renren.modules.knowledge.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.knowledge.entity.KnowEntity;
import io.renren.modules.knowledge.service.KnowService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 11:26:11
 */
@RestController
@RequestMapping("knowledge/know")
public class KnowController {
    @Autowired
    private KnowService knowService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("knowledge:know:list")
    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = knowService.queryPage(params);
        PageUtils page = knowService.getPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{knowId}")
    @RequiresPermissions("knowledge:know:info")
    public R info(@PathVariable("knowId") Long knowId){
		KnowEntity know = knowService.getById(knowId);

        return R.ok().put("know", know);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("knowledge:know:save")
    public R save(@RequestBody KnowEntity know){
		knowService.save(know);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("knowledge:know:update")
    public R update(@RequestBody KnowEntity know){
		knowService.updateById(know);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("knowledge:know:delete")
    public R delete(@RequestBody Long[] knowIds){
		knowService.removeByIds(Arrays.asList(knowIds));

        return R.ok();
    }

}
