package io.renren.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 11:26:11
 */
@Data
@TableName("mind_know")
public class KnowEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 知识id
	 */
	@TableId
	private Long knowId;
	/**
	 * 标题
	 */
	private String knowTitle;
	/**
	 * 内容
	 */
	private String knowContent;
	/**
	 * 管理员id
	 */
	private Long adminId;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
