package io.renren.modules.school.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.school.entity.SchoolEntity;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-16 12:58:49
 */
public interface SchoolService extends IService<SchoolEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

