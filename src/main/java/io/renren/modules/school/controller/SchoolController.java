package io.renren.modules.school.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.school.entity.SchoolEntity;
import io.renren.modules.school.service.SchoolService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-16 12:58:49
 */
@RestController
@RequestMapping("school/school")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("school:school:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = schoolService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{schoolId}")
    @RequiresPermissions("school:school:info")
    public R info(@PathVariable("schoolId") Long schoolId){

		SchoolEntity school = schoolService.getById(schoolId);

        return R.ok().put("school", school);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("school:school:save")
    public R save(@RequestBody SchoolEntity school){
		schoolService.save(school);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("school:school:update")
    public R update(@RequestBody SchoolEntity school){
		schoolService.updateById(school);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("school:school:delete")
    public R delete(@RequestBody Long[] schoolIds){
		schoolService.removeByIds(Arrays.asList(schoolIds));

        return R.ok();
    }

}
