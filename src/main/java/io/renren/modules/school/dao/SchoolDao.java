package io.renren.modules.school.dao;

import io.renren.modules.school.entity.SchoolEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-16 12:58:49
 */
@Mapper
public interface SchoolDao extends BaseMapper<SchoolEntity> {
	SchoolEntity selectById(Integer schoolId);
}
