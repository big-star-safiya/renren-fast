package io.renren.modules.student.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.student.entity.StudentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.student.entity.StudentEntityVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-16 22:34:43
 */
@Mapper
public interface StudentDao extends BaseMapper<StudentEntity> {

    IPage<StudentEntityVO> getPage(IPage<StudentEntityVO> page, @Param("studentName") String studentName, @Param("teacherName") String teacherName, @Param("schoolName") String schoolName);

}
