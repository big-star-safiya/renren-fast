package io.renren.modules.student.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.student.entity.StudentEntity;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-16 22:34:43
 */
public interface StudentService extends IService<StudentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);


}

