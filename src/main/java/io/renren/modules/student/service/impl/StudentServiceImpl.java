package io.renren.modules.student.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.student.entity.StudentEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.student.dao.StudentDao;
import io.renren.modules.student.entity.StudentEntity;
import io.renren.modules.student.service.StudentService;


@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentDao, StudentEntity> implements StudentService {

    @Autowired
    StudentDao studentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentEntity> page = this.page(
                new Query<StudentEntity>().getPage(params),
                new QueryWrapper<StudentEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String teachername = params.get("teachername").toString();
        String schoolname = params.get("schoolname").toString();
        String studentname = params.get("studentname").toString();
        Page<StudentEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<StudentEntityVO> getUser = studentDao.getPage(page, studentname, teachername, schoolname);
        return new PageUtils(getUser);
    }
}