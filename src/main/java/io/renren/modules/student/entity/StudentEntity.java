package io.renren.modules.student.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-16 22:34:43
 */
@Data
@TableName("mind_student")
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 学生id
	 */
	@TableId
	private Long studentId;
	/**
	 * 学生姓名
	 */
	private String studentname;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 外键导员
	 */
	private Long teacherId;
	/**
	 * 外键学校
	 */
	private Long schoolId;


}
