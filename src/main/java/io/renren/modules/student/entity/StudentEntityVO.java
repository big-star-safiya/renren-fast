package io.renren.modules.student.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.renren.modules.school.entity.SchoolEntity;
import lombok.Data;

/**
 *
 *
 * @author
 * @email
 * @date 2023-04-16 22:34:43
 */
@Data
@TableName("mind_student")
public class StudentEntityVO extends StudentEntity implements Serializable {
    private String teachername;
    private String schoolname;

}
