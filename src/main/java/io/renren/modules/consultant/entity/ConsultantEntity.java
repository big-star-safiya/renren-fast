package io.renren.modules.consultant.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 01:30:17
 */
@Data
@TableName("mind_consultant")
public class ConsultantEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 咨询师id
	 */
	@TableId
	private Long conId;
	/**
	 * 咨询师名
	 */
	private String conname;
	/**
	 * 基本信息
	 */
	private String conInf;
	/**
	 * 联系电话
	 */
	private String conTel;
	/**
	 * 最新修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	/**
	 * 外键学校id
	 */
	private Long schoolId;

}
