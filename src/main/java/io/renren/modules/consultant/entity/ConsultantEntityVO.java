package io.renren.modules.consultant.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 *
 * @author
 * @email
 * @date 2023-04-17 01:30:17
 */
@Data
@TableName("mind_consultant")
public class ConsultantEntityVO extends ConsultantEntity implements Serializable {
    private String schoolname;
}
