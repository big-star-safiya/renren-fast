package io.renren.modules.consultant.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.consultant.entity.ConsultantEntity;
import io.renren.modules.consultant.service.ConsultantService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 01:30:17
 */
@RestController
@RequestMapping("consultant/consultant")
public class ConsultantController {
    @Autowired
    private ConsultantService consultantService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("consultant:consultant:list")
    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = consultantService.queryPage(params);
        PageUtils page = consultantService.getPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{conId}")
    @RequiresPermissions("consultant:consultant:info")
    public R info(@PathVariable("conId") Long conId){
		ConsultantEntity consultant = consultantService.getById(conId);

        return R.ok().put("consultant", consultant);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("consultant:consultant:save")
    public R save(@RequestBody ConsultantEntity consultant){
		consultantService.save(consultant);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("consultant:consultant:update")
    public R update(@RequestBody ConsultantEntity consultant){
		consultantService.updateById(consultant);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("consultant:consultant:delete")
    public R delete(@RequestBody Long[] conIds){
		consultantService.removeByIds(Arrays.asList(conIds));

        return R.ok();
    }

}
