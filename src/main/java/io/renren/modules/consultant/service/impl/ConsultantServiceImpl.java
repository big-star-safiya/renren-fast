package io.renren.modules.consultant.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.consultant.entity.ConsultantEntityVO;
import io.renren.modules.student.entity.StudentEntityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.consultant.dao.ConsultantDao;
import io.renren.modules.consultant.entity.ConsultantEntity;
import io.renren.modules.consultant.service.ConsultantService;


@Service("consultantService")
public class ConsultantServiceImpl extends ServiceImpl<ConsultantDao, ConsultantEntity> implements ConsultantService {

    @Autowired
    ConsultantDao consultantDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ConsultantEntity> page = this.page(
                new Query<ConsultantEntity>().getPage(params),
                new QueryWrapper<ConsultantEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getPage(Map<String, Object> params) {
        int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
        int pageSize = Integer.parseInt(String.valueOf(params.get("limit")));
        String conname = params.get("conname").toString();
        String schoolname = params.get("schoolname").toString();
        Page<ConsultantEntityVO> page = new Page<>(pageNum, pageSize);
        IPage<ConsultantEntityVO> getUser = consultantDao.getPage(page, conname, schoolname);
        return new PageUtils(getUser);
    }

}