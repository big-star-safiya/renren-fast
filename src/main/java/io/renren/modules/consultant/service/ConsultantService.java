package io.renren.modules.consultant.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.consultant.entity.ConsultantEntity;

import java.util.Map;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-04-17 01:30:17
 */
public interface ConsultantService extends IService<ConsultantEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getPage(Map<String, Object> params);
}

