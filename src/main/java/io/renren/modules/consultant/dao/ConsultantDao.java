package io.renren.modules.consultant.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.consultant.entity.ConsultantEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.consultant.entity.ConsultantEntityVO;
import io.renren.modules.student.entity.StudentEntityVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-04-17 01:30:17
 */
@Mapper
public interface ConsultantDao extends BaseMapper<ConsultantEntity> {

    IPage<ConsultantEntityVO> getPage(IPage<ConsultantEntityVO> page, @Param("conName") String conName, @Param("schoolName") String schoolName);

}
