package io.renren.modules.qtype.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.qtype.entity.QtypeEntity;
import io.renren.modules.qtype.service.QtypeService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 问题类型管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 16:59:27
 */
@RestController
@RequestMapping("qtype/qtype")
public class QtypeController {
    @Autowired
    private QtypeService qtypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qtype:qtype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qtypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qtypeId}")
    @RequiresPermissions("qtype:qtype:info")
    public R info(@PathVariable("qtypeId") Long qtypeId){
		QtypeEntity qtype = qtypeService.getById(qtypeId);

        return R.ok().put("qtype", qtype);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qtype:qtype:save")
    public R save(@RequestBody QtypeEntity qtype){
		qtypeService.save(qtype);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qtype:qtype:update")
    public R update(@RequestBody QtypeEntity qtype){
		qtypeService.updateById(qtype);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qtype:qtype:delete")
    public R delete(@RequestBody Long[] qtypeIds){
		qtypeService.removeByIds(Arrays.asList(qtypeIds));

        return R.ok();
    }

}
