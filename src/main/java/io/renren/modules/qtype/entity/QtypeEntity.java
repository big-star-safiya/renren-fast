package io.renren.modules.qtype.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 问题类型管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 16:59:27
 */
@Data
@TableName("mind_qtype")
public class QtypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 类型id
	 */
	@TableId
	private Long qtypeId;
	/**
	 * 问题类型
	 */
	private String qtypename;
	/**
	 * 安全分数线
	 */
	private Integer safeScore;
	/**
	 * 安全分数线信息
	 */
	private String safeScoreInfo;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)//该注解表示在插入和修改数据时开启自动生成
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

}
