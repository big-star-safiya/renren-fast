package io.renren.modules.qtype.dao;

import io.renren.modules.qtype.entity.QtypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 问题类型管理
 * 
 * @author 
 * @email 
 * @date 2023-04-17 16:59:27
 */
@Mapper
public interface QtypeDao extends BaseMapper<QtypeEntity> {
	
}
