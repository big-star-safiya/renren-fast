package io.renren.modules.qtype.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.qtype.entity.QtypeEntity;

import java.util.Map;

/**
 * 问题类型管理
 *
 * @author 
 * @email 
 * @date 2023-04-17 16:59:27
 */
public interface QtypeService extends IService<QtypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

