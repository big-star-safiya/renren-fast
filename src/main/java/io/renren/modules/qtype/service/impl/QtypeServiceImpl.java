package io.renren.modules.qtype.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.qtype.dao.QtypeDao;
import io.renren.modules.qtype.entity.QtypeEntity;
import io.renren.modules.qtype.service.QtypeService;


@Service("qtypeService")
public class QtypeServiceImpl extends ServiceImpl<QtypeDao, QtypeEntity> implements QtypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QtypeEntity> page = this.page(
                new Query<QtypeEntity>().getPage(params),
                new QueryWrapper<QtypeEntity>().like("qtypename", params.get("qtypename"))
        );

        return new PageUtils(page);
    }

}