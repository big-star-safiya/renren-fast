CREATE TABLE `renrenfast`.`mind_school` (
    `school_id` BIGINT NOT NULL AUTO_INCREMENT,
    `schoolname` VARCHAR(16) NOT NULL,
    `create_user_id` BIGINT NOT NULL,
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`school_id`),
    UNIQUE INDEX `schoolname_UNIQUE` (`schoolname` ASC) VISIBLE);